/*
** map2.c for map2.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Mar 18 01:56:45 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 17:29:42 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	center_line(int line)
{
  int	cente_line_inf;
  int	cente_line_sup;

  cente_line_inf = 0;
  cente_line_sup = 0;
  cente_line_inf = (LINES / 2) - (line / 2);
  cente_line_sup = (LINES / 2) + (line / 2);
  if (cente_line_inf > 0 && cente_line_sup < LINES)
    return (cente_line_inf);
  else
    return (-1);
  return (0);
}

int	center_col(int col)
{
  int	cente_col_inf;
  int	cente_col_sup;

  cente_col_inf = 0;
  cente_col_sup = 0;
  cente_col_inf = (COLS / 2) - (col / 2);
  cente_col_sup = (COLS / 2) + (col / 2);
  if (cente_col_inf > 0 && cente_col_sup < COLS)
    return (cente_col_inf);
  else
    return (-1);
  return (0);
}

void		display_col(int col, int fin, int depart, s_imap *info_map)
{
  static int	inc = 0;
  int		max_col;

  max_col = depart + col;
  while (depart < max_col)
    {
      if (inc == 0)
        inc++;
      info_map->y_inf = fin;
      mvprintw(fin, depart, "-");
      depart++;
    }
  if (inc == 1)
    {
      info_map->y_sup = fin;
      mvprintw(fin, depart, "\n");
      inc++;
    }
}

void	display_line(int l, s_opt option, int c, s_imap *info_map)
{
  int	max_line;
  int	col_d;

  max_line = l + option.map_row;
  col_d = c + option.map_col - 1;
  info_map->x_inf = c;
  info_map->x_sup = col_d;
  while (l < max_line - 2)
    {
      mvprintw(l, c, "|");
      refresh();
      mvprintw(l, col_d, "|");
      l++;
    }
  display_col(option.map_col, l, c, info_map);
  refresh();
}

int	disp_map(s_opt option, s_imap *info_map, s_disp *disp)
{
  tet_aff();
  if ((disp->c = center_col(option.map_col)) == -1)
    return (-1);
  if ((disp->l = center_line(option.map_row)) == -1)
    return (-1);
  display_col(option.map_col, disp->l - 1, disp->c, info_map);
  display_line(disp->l, option, disp->c, info_map);
  mode_canonique(0);
  return (0);
}
