/*
** check_tetriminos.c for check_tetriminos in /home/maurin_b/rendu/PSU/PSU_2015_tetris/src
** 
** Made by thomas maurin
** Login   <maurin_b@epitech.net>
** 
** Started on  Fri Mar 18 14:49:43 2016 thomas maurin
** Last update Sun Mar 20 21:49:47 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int		open_tetriminos(char *s, t_form **tetrim, int debug)
{
  int		fd;
  s_infos	infos;
  int		val;
  char		**form;

  form = NULL;
  infos.debug = debug;
  if ((infos.name = parse_name(s, 0)) == NULL && (infos.haut = 0) == 0)
    return (-2);
  fd = open(concatain_open(s), O_RDONLY);
  if ((val = check_info_tetrimino(get_next_line(fd), &infos)) != 1)
    return (val);
  if ((form = malloc(sizeof(char **) * (infos.haut + 2))) == NULL)
    return (-1);
  while (42)
    {
      val = do_while_open_tet(fd, tetrim, form, &infos);
      if (val == -1)
        return (-1);
      else if (val == -2)
	return (-2);
      else if (val == 201)
        return (1);
    }
  return (0);
}

int		nb_tetriminos(int pass)
{
  struct dirent	*di;
  DIR		*pdir;
  int		i;

  i = 0;
  pdir = opendir("tetriminos");
  while ((di = readdir(pdir)) != NULL)
    {
      if (my_strstr(di->d_name, ".", 1) == 0 &&
	  my_strstr(di->d_name, "..", 2) == 0 && parse_name_nb(di->d_name, 0) == 1)
        i++;
    }
  if (i == 0 && pass == 1)
    my_printf("Tetriminos : %d\n", i);
  if (i == 0)
    exit_function(-10, NULL);
  return (i);
}

int		recup_tet(DIR *pdir, int debug, t_form **tetrim, int nb)
{
  int		i;
  struct dirent	*di;
  int		val;

  i = 0;
  while ((di = readdir(pdir)) != NULL)
    {
      val = 0;
      if (my_strstr(di->d_name, ".", 1) == 0 &&
	  my_strstr(di->d_name, "..", 2) == 0 && parse_name_nb(di->d_name, 0) == 1)
        {
          if ((val = open_tetriminos(di->d_name, tetrim, debug)) == -1)
	    return (-1);
        }
      if (val == -2)
	i++;
    }
   if (i == nb)
     exit_function(-10, NULL);
  return (0);
}

t_form		*recup_tetriminos(int debug)
{
  DIR		*pdir;
  t_form	*tetrim;
  t_form	*tmp;
  s_infos	infos;
  int		i;
  int		nb;

  i = 0;
  tetrim = NULL;
  pdir = opendir("tetriminos");
  if (pdir == NULL)
    return (NULL);
  if (debug == 1)
    i = 1;
  nb = nb_tetriminos(i);
  if (debug == 1)
    my_printf("Tetriminos : %d\n", nb);
  i = 0;
  if (recup_tet(pdir, debug, &tetrim, nb) == -1)
    return (NULL);
  tmp = fill_list_form(&tetrim, infos, NULL, 1);
  tetrim = fill_list_last_step(tmp, tetrim);
  return (tetrim);
}
