/*
** tetris.c for tetris.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Feb 22 15:06:07 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 22:02:56 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	tet_aff()
{
  int	y;

  y = 5;
  init_pair(3, COLOR_GREEN, COLOR_BLACK);
  attron(COLOR_PAIR(3));
  mvprintw(y, 1,
           "#####  ####  ##### ####  #####  ####");
  mvprintw(y + 1,1,
           "  #    #       #   #  #    #    #   ");
  mvprintw(y + 2, 1,
           "  #    ####    #   ####    #    ####");
  mvprintw(y + 3, 1,
           "  #    #       #   ##      #       #");
  mvprintw(y + 4, 1,
           "  #    ####    #   # #   #####  ####");
  attroff(COLOR_PAIR(3));
}

void	loose_aff()
{
  int	y;

  y = LINES / 2 - 10;
  init_pair(1, COLOR_RED, COLOR_BLACK);
  attron(COLOR_PAIR(1));
  mvprintw(y, (COLS / 2) - 23,
           "#   #  ###  #  #     #    ###  ###  ####  ###   #");
  mvprintw(y + 1, (COLS / 2) - 23,
           "#  #   # #  #  #     #    # #  # #  #     #     #");
  mvprintw(y + 2, (COLS / 2) - 23,
           "###    # #  #  #     #    # #  # #  ####  ###   #");
  mvprintw(y + 3, (COLS / 2) - 23,
           "#      # #  #  #     #    # #  # #     #  #      ");
  mvprintw(y + 4, (COLS / 2) - 23,
           "#      ###  ####     ###  ###  ###  ####  ###   #");
  attroff(COLOR_PAIR(1));
}

int	main(int ac, char **av, char **env)
{
  s_opt	option;

  if (env == NULL || !env)
    return (-1);
  setupterm(NULL, 1, (int *)0);
  if (init_options(&option) == -1)
    return (-1);
  option.bin = av[0];
  if (ac > 1)
    manage_options(ac, av, &option);
  if (option.debug_mode == 1)
    aff_debug(&option);
  window(option);
  return (0);
}
