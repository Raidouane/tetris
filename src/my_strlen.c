/*
** my_strlen.c for my_strlen in /home/da-fon_s/rendu/Piscine_C_J06
** 
** Made by samuel da-fonseca
** Login   <da-fon_s@epitech.net>
** 
** Started on  Tue Oct  6 11:12:09 2015 samuel da-fonseca
** Last update Fri Mar 18 21:54:07 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str && str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}

int	my_strlen_tet(char *str)
{
  int	len;

  len = 0;
  while (str && str[len] != '\0' && str[len] == ' ')
    len++;
  if (str[len] == '*')
    return (len);
  return (-1);
}
