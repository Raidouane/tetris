/*
** my_putstr.c for my_putstr in /home/da-fon_s/rendu/Piscine_C_J06
** 
** Made by samuel da-fonseca
** Login   <da-fon_s@epitech.net>
** 
** Started on  Tue Oct  6 11:11:39 2015 samuel da-fonseca
** Last update Fri Mar 18 16:16:39 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str && str[i] != '\0')
    {
      my_putchar(str[i]);
      i = i + 1;
    }
}

void	my_putstr_spec(char *str)
{
  int	i;

  i = 1;
  while (str && str[i] != '\0')
    {
      my_putchar(str[i]);
      i = i + 1;
    }
  my_putchar('\n');
}
