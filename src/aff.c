/*
** aff.c for aff.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Mar 18 02:03:22 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 17:17:04 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

t_aff		*list_aff(t_aff *la, t_form *form, int x, int y)
{
  t_aff		*elem;

  if ((elem = malloc(sizeof(t_aff *) *
                     size_doublestring(form->form) * 100)) == NULL)
    return (NULL);
  elem->color = form->color;
  elem->form = form->form;
  elem->x = x;
  elem->y = y;
  elem->next = la;
  if (la != NULL)
    la->prev = elem;
  la = elem;
  la->prev = NULL;
  return (la);
}

void	aff_first_tet(t_aff *aff, int a, int u, int d)
{
  while (aff->form[a] != NULL)
    {
      u = 0;
      d = 0;
      while (aff->form[a][u] != '\0')
        {
	  while (aff->form[a][u] == ' ' && aff->form[a][u] != '\0')
            {
              u++;
              d++;
            }
          mvprintw(aff->y + a, aff->x + d, "%c", aff->form[a][u]);
          d++;
          u++;
        }
      a++;
    }
}

t_aff	*aff_tet(t_aff *aff, int a, int u, int d)
{
  while ((aff)->next != NULL)
    {
      a = 0;
      while ((aff)->form[a] != NULL)
        {
          u = 0;
          d = 0;
          while ((aff)->form[a][u] != '\0')
            {
	      while (aff->form[a][u] == ' ' && aff->form[a][u] != '\0')
                {
                  u++;
                  d++;
		}
              mvprintw((aff)->y + a, (aff)->x + d, "%c", (aff)->form[a][u]);
              d++;
              u++;
            }
          a++;
        }
      aff = (aff)->next;
    }
  return (aff);
}

void	disp_form_in_progress(s_disp *disp)
{
  int	i;
  int	d;
  int	u;

  i = 0;
  while (disp->form->form[i] != NULL)
    {
      u = 0;
      d = 0;
      while (disp->form->form[i][u] != '\0')
	{
	  while (disp->form->form[i][u] == ' ' && disp->form->form[i][u] != '\0')
	    {
	      u++;
	      d++;
	    }
	  mvprintw(disp->y + i, disp->x + d, "%c", disp->form->form[i][u]);
	  d++;
	  u++;
	}
      i++;
    }
}

int	display(s_disp  *disp, t_aff *tmp, int *quad)
{
  if (loose(quad, disp->opt.map_col) == -1)
    {
      loose_aff();
      refresh();
      sleep(3);
      return (-1);
    }
  if (disp->pause == 1)
    {
      mode_canonique(1);
      pause_tet();
      mode_canonique(0);
      disp->pause = 0;
    }
  disp_form_in_progress(disp);
  disp->aff = tmp;
  if (disp->aff != NULL)
    {
      disp->aff = aff_tet(disp->aff, 0, 0, 0);
      aff_first_tet(disp->aff, 0, 0, 0);
    }
  refresh();
  return (0);
}
