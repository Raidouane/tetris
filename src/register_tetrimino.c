/*
** register_tetrimino.c for register_termino.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Feb 24 15:01:41 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 21:19:55 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

t_form	*fill_list_last_step(t_form *tmp, t_form *la)
{
  tmp->next = la;
  la->prev = tmp;
  return (la);
}

int	size_doublestring(char **form)
{
  int	i;
  int	len;

  i = 0;
  len = 0;
  while (form[i] != NULL)
    {
      len = my_strlen(form[i]) + len;
      i++;
    }
  return (len + 1);
}

t_form		*fill_list_form(t_form **la, s_infos infos, char **form, int pass)
{
  t_form	*elem;
  static t_form	*tmp = NULL;
  static int	i = 0;

  if (pass == 1)
    return (tmp);
  if ((elem = malloc(sizeof(t_form *) * size_doublestring(form) * 100)) == NULL)
    return (NULL);
  elem->id = i;
  elem->name = infos.name;
  elem->larg = infos.larg;
  elem->haut = infos.haut;
  elem->color = infos.color;
  elem->turned = 0;
  elem->form = my_double_str_cpy(elem->form, form);
  elem->next = *la;
  if (*la != NULL)
    (*la)->prev = elem;
  *la = elem;
  if ((*la)->next == NULL)
    tmp = *la;
  if (form && form != NULL)
    free(form);
  i++;
  return (NULL);
}



int	disp_recup_tet(s_infos *infos, int *i)
{
  *i = 0;
  if (infos->debug == 1)
    my_printf("Tetriminos : Name %s : Error\n", infos->name);
  return (-2);
}

int		do_while_open_tet(int fd, t_form **tetrim, char **form, s_infos *infos)
{
  char		*tet;
  int		val;
  static int	i = 0;

  tet = NULL;
  tet = get_next_line(fd);
  if (tet == NULL && i != infos->haut)
    return (disp_recup_tet(infos, &i));
  if (tet == NULL)
    {
      i = 0;
      fill_list_form(tetrim, *infos, form, 0);
      return (201);
    }
  val = check_error_form(infos, tet, form);
  if (val == -1)
    {
      i = 0;
      return (-1);
    }
  if (val == -2)
    return (disp_recup_tet(infos, &i));
  i++;
  return (0);
}
