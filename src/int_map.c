/*
** int_map.c for int_map.c in /home/maurin_b/rendu/PSU_2015_tetris/thomas
** 
** Made by thomas maurin
** Login   <maurin_b@epitech.net>
** 
** Started on  Wed Feb 24 14:58:24 2016 thomas maurin
** Last update Sun Mar 20 21:33:49 2016 Raidouane EL MOUKHTARI
*/
#include "my.h"

int	parse_name_nb_next(char *s, int i, int t)
{
  char	*s1;

  s1 = NULL;
  s1 = my_dup_strcpy(s1, s);
  while (t < i)
    {
      t++;
      s1++;
    }
  if (my_strstr(s1, ".tetrimino", 10) == 1)
    return (1);
  else
    return (parse_name_nb(s, i + 1));
  return (-1);
}

int	parse_name_nb(char *s, int i)
{
  int	t;
  int	len;

  t = 0;
  len = 0;
  if (i >= my_strlen(s))
    return (-1);
  while (s && s[i] != '\0' && s[i] != '.')
    i++;
  t = i;
  while (s && s[t] != '\0' && s[t])
    {
      t++;
      len++;
    }
  if (len == 10)
    return (parse_name_nb_next(s, i, 0));
  else
    return (parse_name_nb(s, i + 1));
  return (1);
}


int		*bordures(int *c, int size, int *int_map, int map_col)
{
  static int	check = 0;
  static int	i = 0;

  if (i == 0)
    {
      check = map_col;
      i++;
    }
  if (*c == check)
    {
      int_map[*c - 1] = 0;
      int_map[*c] = 0;
      check += map_col;
    }
  else if (*c == (size - map_col - 1))
    {
      while (*c <= size)
	{
	  int_map[*c] = 0;
	  (*c)++;
	}
    }
  else
    int_map[*c] = 2;
  return (int_map);
}

int	*int_map(s_opt option)
{
  int		*int_map;
  int		c;
  int		size;
  int		map_col;

  c = 0;
  size = option.map_col * option.map_row;
  if ((int_map = malloc(sizeof(int *) * (size + 1))) == NULL)
    return (NULL);
  map_col = option.map_col;
  while (c < size)
    {
      while (c < map_col)
	{
	  int_map[c] = 0;
	  c++;
	}
      int_map = bordures(&c, size, int_map, map_col);
      c++;
    }
  return (int_map);
}
