/*
** my_doublestr_cpy.c for my_doublestr_cpy.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Mar  2 17:01:36 2016 Raidouane EL MOUKHTARI
** Last update Fri Mar 18 22:08:16 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	**my_double_str_cpy(char **dest, char **src)
{
  int	size;
  int	i;

  size = 0;
  i = 0;
  dest = NULL;
  while (src[size++] != NULL);
  if ((dest = malloc(sizeof(char **) * (size + 2))) == NULL)
    return (NULL);
  while (src && src[i] != NULL)
    {
      dest[i] = NULL;
      if ((dest[i] = malloc(sizeof(char *) * (my_strlen(src[i]) + 2))) == NULL)
	return (NULL);
      dest[i] = my_strcpy(dest[i], src[i]);
      i++;
    }
  dest[i] = NULL;
  return (dest);
}
