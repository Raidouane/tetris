/*
** check_infos.c for check_infos in /home/maurin_b/rendu/PSU/PSU_2015_tetris/src
** 
** Made by thomas maurin
** Login   <maurin_b@epitech.net>
** 
** Started on  Fri Mar 18 14:34:16 2016 thomas maurin
** Last update Sun Mar 20 21:00:08 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void		attribute_infos(char *nb, int *larg, int *haut, int *color)
{
  static int	next = 0;

  if (next == 0 && (*larg = my_getnbr(nb)) != 0)
    {
      next++;
      free(nb);
    }
  else if (next == 1 && (*haut = my_getnbr(nb)) != 0)
    {
      next++;
      free(nb);
    }
  else if (next == 2 && (*color = my_getnbr(nb)) != 0)
    {
      next = 0;
      free(nb);
    }
}


void	parse_infos_second(char *s, int *inc, int *i)
{
  while (*inc < 1 && s && s[*i] != '\0' && s[*i] != ' ')
    {
      (*i)++;
      (*inc)++;
    }
  if (s[*i] == ' ')
    (*i)++;
}

int		parse_infos(char *s, int *larg, int *haut, int *color)
{
  char		*nb;
  int		i;
  int		size;
  int		t;
  static int	inc = 0;

  i = 0;
  while (s && s[i] != '\0')
    {
      size = 0;
      if ((nb = NULL) == NULL && (size = len_malloc(s, i)) == 0)
        return (-1);
      if ((t = 0) == 0 && (nb = malloc(sizeof(char *) * size + 1)) == NULL)
        return (-1);
      while (s && s[i] != '\0' && s[i] != ' ')
        {
          nb[t] = s[i];
          t++;
          i++;
        }
      nb[t] = '\0';
      attribute_infos(nb, larg, haut, color);
      parse_infos_second(s, &inc, &i);
    }
  return (0);
}

int	check_info_tetrimino(char *s, s_infos *infos)
{
  infos->haut = 0;
  if (count_spaces(s) != 2)
    {
      if (infos->debug == 1)
        my_printf("Tetriminos : Name %s : Error\n", infos->name);
      return (-2);
    }
  else
    {
      if (all_num_spaces(s) == -1)
        {
          if (infos->debug == 1)
            my_printf("Tetriminos : Name %s : Error\n", infos->name);
          return (-2);
        }
      else
        {
          if (parse_infos(s, &(infos->larg),
			  &(infos->haut), &(infos->color)) == -1)
            return (-1);
          else
            return (1);
        }
    }
  return (0);
}
