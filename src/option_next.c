/*
** option_next.c for option_next.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Mar 18 00:51:14 2016 Raidouane EL MOUKHTARI
** Last update Fri Mar 18 16:24:01 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	aff_debug_next_next(s_opt *option)
{
  if (my_strstr(option->keyquit, " ", 1) == 1)
    my_printf("Key Quit : (space)\n");
  else
    my_printf("Key Quit : %s\n", option->keyquit);
  if (my_strstr(option->keypause, " ", 1) == 1)
    my_printf("Key Pause : (space)\n");
  else
    my_printf("Key Pause : %s\n", option->keypause);
  if (option->hide_next == 1)
    my_printf("Next : Yes\n");
  else
    my_printf("Next : No\n");
  my_printf("Level : %d\n", option->level);
  if (option->map_row == 0 && option->map_col == 0)
    my_printf("Size : 20*10\n");
  else
    my_printf("Size : %d*%d\n", option->map_row, option->map_col);
}

void	aff_debug_next(s_opt *option)
{
  if (my_strstr(option->keyturn, " ", 1) == 1)
    my_printf("Key Turn : (space)\n");
  else if (option->kt == 0)
    {
      my_printf("Key Turn : ^E");
      my_putstr_spec(option->keyturn);
    }
  else
    my_printf("Key Turn : %s\n", option->keyturn);
  if (my_strstr(option->keydrop, " ", 1) == 1)
    my_printf("Key Drop : (space)\n");
  else if (option->kd == 0)
    {
      my_printf("Key Drop : ^E");
      my_putstr_spec(option->keydrop);
    }
  else
    my_printf("Key Drop : %s\n", option->keydrop);
  aff_debug_next_next(option);
}

void	aff_debug(s_opt *option)
{
  my_printf("*** DEBUG MODE ***\n");
  if (my_strstr(option->keyleft, " ", 1) == 1)
    my_printf("Key Left : (space)\n");
  else if (option->kl == 0)
    {
      my_printf("Key Left : ^E");
      my_putstr_spec(option->keyleft);
    }
  else
    my_printf("Key Left : %s\n", option->keyleft);
  if (my_strstr(option->keyright, " ", 1) == 1)
    my_printf("Key Right : (space)\n");
  else if (option->kr == 0)
    {
      my_printf("Key Right : ^E");
      my_putstr_spec(option->keyright);
    }
  else
    my_printf("Key Right : %s\n", option->keyright);
  aff_debug_next(option);
}

int	fill_resize_option(char *s, int i, s_opt *option, int size)
{
  int		t;
  static int	pass = 0;
  char		*nb;

  nb = NULL;
  t = 0;
  if ((nb = malloc(sizeof(char *) * size)) == NULL)
    return (-1);
  while (s[i] != ',' && s[i] != '\0')
    {
      nb[t] = s[i];
      t++;
      i++;
    }
  nb[t] = '\0';
  if (pass == 0 && check_num(nb) == 1)
    {
      option->map_row = my_getnbr(nb);
      pass = 1;
    }
  else if (pass == 1 && check_num(nb) == 1)
    option->map_col = my_getnbr(nb);
  free(nb);
  return (1);
}

int	manage_resize(char *s, s_opt *option)
{
  int	i;
  int	t;

  i = 0;
  t = 0;
  while (s[i++] != '\0' && s && s[i] != '=');
  if (s[i] == '=')
    {
      t = i;
      while (s[t] != ',')
        t++;
      if (s[t] == ',')
        {
          if (fill_resize_option(s, i + 1, option, t - i) == -1)
            return (-1);
          if (fill_resize_option(s, t + 1, option, my_strlen(s) - t) == -1)
            return (-1);
        }
      else
        return (-1);
    }
  else
    return (-1);
  return (0);
}
