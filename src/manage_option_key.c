/*
** manage_option_key.c for manage_option_key.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Feb 23 01:18:18 2016 Raidouane EL MOUKHTARI
** Last update Sat Mar 19 19:20:41 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	fill_keyoption(char *s, s_opt *option, int key)
{
  if (key == 2 || key == 3)
    {
      option->keyleft = my_dup_strcpy(option->keyleft, s);
      option->kl = 1;
    }
  if (key == 4 || key == 5)
    {
      option->keyright = my_dup_strcpy(option->keyright, s);
      option->kr = 1;
    }
  if (key == 6 || key == 7)
    {
      option->keyturn = my_dup_strcpy(option->keyturn, s);
      option->kt = 1;
    }
  if (key == 10 || key == 11)
    option->keyquit = my_dup_strcpy(option->keyquit, s);
  if (key == 12 || key == 13)
    option->keypause = my_dup_strcpy(option->keypause, s);
  if (key == 8 || key == 9)
    {
      option->keydrop = my_dup_strcpy(option->keydrop, s);
      option->kd = 1;
    }
}

int	manage_opt_next_key(char *s, s_opt *option, int key)
{
  int	i;
  char	opt[256];
  int	t;

  t = 0;
  i = 0;
  if (my_strlen(s) < 12)
    exit_function(20, option->bin);
  while (s[i++] != '\0' && s && s[i] != '=');
  if (s[i] == '=')
    {
      i++;
      while (s && s[i] != '\0')
	{
	  opt[t] = s[i];
	  t++;
	  i++;
	}
      opt[t] = '\0';
      fill_keyoption(opt, option, key);
    }
  else
    return (-1);
  return (0);
}

int	manage_key(char *s, char *s1, s_opt *option, int key)
{
  if (s1 == NULL && manage_opt_next_key(s, option, key) == -1)
    return (-1);
  else if (s1 != NULL)
    fill_keyoption(s1, option, key);
  return (0);
}
