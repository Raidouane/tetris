/*
** manage_option_level.c for manage_option_level.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Feb 23 00:52:36 2016 Raidouane EL MOUKHTARI
** Last update Fri Feb 26 00:41:48 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	fill_level_option(char *s, int t, char *nb, s_opt *option)
{
  int	i;

  i = t;
  t = 0;
  while (s[i] != '\0' && s && s[i] >= '0')
    {
      nb[t] = s[i];
      t++;
      i++;
    }
  nb[t] = '\0';
  if (check_num(nb) == 1)
    option->level = my_getnbr(nb);
}

int	manage_opt_next_level(char *s, s_opt *option)
{
  int	i;
  int	t;
  char	*nb;

  i = 0;
  while (s[i++] != '\0' && s && s[i] != '=');
  if (s[i] == '=')
    {
      t = i;
      while (s[i] != '\0' && s)
        i++;
      if ((nb = malloc(sizeof(char *) * (i - t))) == NULL)
        return (-1);
      if (s[i] == '\0')
        fill_level_option(s, t + 1, nb, option);
      else
        return (-1);
    }
  else
    return (-1);
  return (0);
}

int	manage_opt_level(char *s, char *s1, int ac, s_opt *option)
{
  if (ac == 1)
    return (manage_opt_next_level(s, option));
  else if (ac == 2)
    {
      if (check_num(s1) == 1 && (option->level = my_getnbr(s1)) == -1)
	return (-1);
    }
  return (0);
}
