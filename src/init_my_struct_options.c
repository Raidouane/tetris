/*
** init_my_struct_options.c for init_my_struct_options.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Feb 23 01:23:36 2016 Raidouane EL MOUKHTARI
** Last update Sat Mar 19 19:17:09 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	init_options_next(s_opt *opt)
{
  opt->kl = 0;
  opt->kr = 0;
  opt->kd = 0;
  opt->kt = 0;
  opt->map_row = 0;
  opt->map_col = 0;
  opt->hide_next = 1;
  opt->debug_mode = 0;
  opt->level = 1;
}

int	init_options(s_opt *opt)
{
  if ((opt->keyleft = malloc(sizeof(char *) * (256))) == NULL ||
      (opt->keyright = malloc(sizeof(char *) * (256))) == NULL)
    return (-1);
  if ((opt->keyturn = malloc(sizeof(char *) * (256))) == NULL ||
      (opt->keydrop = malloc(sizeof(char *) * (256))) == NULL)
    return (-1);
  if ((opt->keyquit = malloc(sizeof(char *) * (256))) == NULL ||
      (opt->keypause = malloc(sizeof(char *) * (256))) == NULL)
    return (-1);
  opt->keyleft = my_strcpy(opt->keyleft, tigetstr("kcub1"));
  opt->keyright = my_strcpy(opt->keyright, tigetstr("kcuf1"));
  opt->keyturn = my_strcpy(opt->keyturn, tigetstr("kcuu1"));
  opt->keyquit = my_strcpy(opt->keyquit, "q");
  opt->keydrop = my_strcpy(opt->keydrop, tigetstr("kcud1"));
  opt->keypause = my_strcpy(opt->keypause, " ");
  init_options_next(opt);
  return (1);
}
