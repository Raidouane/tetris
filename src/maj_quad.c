/*
** maj_quad.c for maj_quad.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Mar 15 13:37:14 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 17:40:17 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	pause_tet()
{
  char	c;
  int	y;

  y = LINES / 2 - 6;
  init_pair(2, COLOR_GREEN, COLOR_RED);
  attron(COLOR_PAIR(2));
  mvprintw(y, (COLS / 2) - 18,
	   "#--------------Pause---------------#");
  mvprintw(y + 1, (COLS / 2) - 18,
	   "#                                  #");
  mvprintw(y + 2, (COLS / 2) - 18,
	   "#                                  #");
  mvprintw(y + 3, (COLS / 2) - 18,
	   "#      Press a key to continue     #");
  mvprintw(y + 4, (COLS / 2) - 18,
	   "#                                  #");
  mvprintw(y + 5, (COLS / 2) - 18,
	   "#                                  #");
  mvprintw(y + 6, (COLS / 2) - 18,
	   "#----------------------------------#");
  attroff(COLOR_PAIR(2));
  refresh();
  mode_canonique_spec(0);
  read(0, &c, 1);
  mode_canonique_spec(1);
}

void	manage_keys_next(char touch[], s_disp *disp)
{
  int	kd;
  int	kp;

  kd = my_strstr(touch, disp->opt.keydrop, my_strlen(disp->opt.keydrop));
  kp = my_strstr(touch, disp->opt.keypause, my_strlen(disp->opt.keypause));
  if (kp == 1)
    disp->pause = 1;
  else if (kd == 1)
    {
      while (check_bottom(disp, disp->quad) == 0)
        (disp->y)++;
    }
}

int	manage_keys(char touch[], s_disp *disp, t_form *form, int x_sup)
{
  int	kl;
  int	kr;
  int	kd;

  kr = my_strstr(touch, disp->opt.keyright, my_strlen(disp->opt.keyright));
  kl = my_strstr(touch, disp->opt.keyleft, my_strlen(disp->opt.keyleft));
  kd = my_strstr(touch, disp->opt.keydrop, my_strlen(disp->opt.keydrop));
  if (disp->x + form->larg < x_sup && kr == 1)
    {
      (disp->x)++;
      (disp->y)--;
    }
  else if (disp->x - 1 > disp->info_map->x_inf && kl == 1)
    {
      (disp->y)--;
      (disp->x)--;
    }
  manage_keys_next(touch, disp);
  if (kd == 0 && (touch[0] == '\0' || kl == 1 || kr == 1))
    (disp->y)++;
  return (0);
}

int	maj_quad(t_form *form, int xy[], s_imap *info_map, int *quad)
{
  int	i;
  int	t;
  int	k;
  int	u;
  int	w;

  k = 0;
  u = 0;
  i = ((info_map->x_sup - info_map->x_inf + 1) *
       (xy[1] - info_map->y_sup)) + (xy[0] - info_map->x_inf);
  w = i;
  while ((t = 0) == 0 && form->form && form->form[k] != NULL)
    {
      while (form->form[k] && form->form[k][t] != '\0')
	{
	  if (form->form[k][t] == '*')
	    quad[i] = 0;
	  i++;
	  t++;
	}
      k++;
      u++;
      i = w + (info_map->x_sup - info_map->x_inf + 1) * u;
    }
  return (0);
}
