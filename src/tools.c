/*
** tools.c for tools.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Feb 23 02:00:58 2016 Raidouane EL MOUKHTARI
** Last update Fri Mar 18 23:07:52 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	check_num(char *s)
{
  int	i;

  i = 0;
  while (s && s[i] >= '0' && s[i] <= '9' && s[i] != '\0')
    i++;
  if (i == my_strlen(s))
    return (1);
  return (-1);
}

int	all_num_spaces(char *s)
{
  int	i;
  int	k;

  i = 0;
  k = 0;
  while (s && s[i] != '\0')
    {
      if (s && s[i] != '\0' && ((s[i] >= '0' && s[i] <= '9') || s[i] == ' '))
	k++;
      i++;
    }
  if (k == i)
    return (1);
  return (-1);
}

int	len_malloc(char *s, int i)
{
  int	len;

  len = 0;
  while (s && s[i] != '\0' && s[i] != ' ')
    {
      len++;
      i++;
    }
  return (len);
}

char	*concatain_open(char *s)
{
  char	*str;

  str = NULL;
  if ((str = malloc(sizeof(char *) * ((my_strlen(s) + 13)))) == NULL)
    return (NULL);
  str = my_strcpy(str, "tetriminos/");
  str = my_strcat(str, s);
  if ((s = malloc(sizeof(char *) * ((my_strlen(str) + 1)))) == NULL)
    return (NULL);
  s = my_strcpy(s, str);
  free(str);
  return (s);
}

int	count_spaces(char *s)
{
  int	i;
  int	space;

  i = 0;
  space = 0;
  while (s && s[i] != '\0')
    {
      if (s[i] == ' ')
	space++;
      i++;
    }
  return (space);
}
