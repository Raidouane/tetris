/*
** check_error.c for check_error.C in /home/maurin_b/rendu/PSU/PSU_2015_tetris/src
** 
** Made by thomas maurin
** Login   <maurin_b@epitech.net>
** 
** Started on  Fri Mar 18 14:43:38 2016 thomas maurin
** Last update Sun Mar 20 21:17:40 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	check_error_form_third(int *k, int haut, int *h, int *pass)
{
  *h = 0;
  if (*k >= haut)
    {
      *k = 0;
      return (-2);
    }
  *pass = 0;
  *k = 0;
  return (0);
}

int	check_error_form_second(char *s, int lh[], int *k, char **form)
{
  if (my_strlen(s) > lh[0])
    {
      if (my_strlen_spaces(s, lh[0]) == -1)
	return (-2);
    }
  if (my_strlen(s) < lh[0])
    (*k)++;
  if (check_line(s) == -1)
    return (-2);
  else
    {
      if ((form[lh[1]] = malloc(sizeof(char **) * (my_strlen(s) + 11))) == NULL)
        return (-1);
      form[lh[1]] = my_strcpy(form[lh[1]], s);
    }
  return (0);
}

int		check_error_form(s_infos *infos, char *s, char **form)
{
  static int	h = 0;
  static int	k = 0;
  static int	pass = 0;
  int		lh[2];

  lh[0] = infos->larg;
  lh[1] = h;
  if (my_strlen_tet(s) > infos->larg)
    return (-2);
  if (check_error_form_second(s, lh, &k, form) == -2)
    return (-2);
  if (h == infos->haut - 1)
    {
      infos->val = 7;
      form[h + 1] = NULL;
      return (check_error_form_third(&k, infos->haut, &h, &pass));
    }
  h++;
  return (0);
}
