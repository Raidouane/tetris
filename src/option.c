/*
** option.c for option.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Feb 23 01:59:38 2016 Raidouane EL MOUKHTARI
** Last update Sat Mar 19 18:38:04 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	manage_options_next(char *s, char *s1, s_opt *option, int t)
{
  int	var;

  var = 0;
  if (s1 == NULL)
    {
      if (my_debstrstr(s, "--level=", 8) == 1)
	var = manage_opt_level(s, NULL, 1, option);
      else if (my_debstrstr(s, "--map-size=", 11) == 1)
	var = manage_resize(s, option);
      else if (my_strstr(s, "--without-next", 14) == 1 ||
	       my_strstr(s, "-w", 2) == 1)
	manage_opt_other(option, 0);
      else if (my_strstr(s, "--debug", 7) == 1 ||
	       my_strstr(s, "-d", 2) == 1)
	manage_opt_other(option, 1);
      else if (my_strstr(s, "--help", 6) == 1)
	aff_help(option->bin);
      else
	var = manage_key(s, NULL, option, t);
    }
  else
    var = racc_opts(s, s1, option, t);
  if (var == -1)
    return (-1);
  return (0);
}

char	**flags_options_next(char **flags)
{
  flags[9] = "--key-drop=";
  flags[10] = "-kq";
  flags[11] = "--key-quit=";
  flags[12] = "-kp";
  flags[13] = "--key-pause=";
  flags[14] = "--map-size=";
  flags[15] = "-w";
  flags[16] = "--without-next";
  flags[17] = "-d";
  flags[18] = "--debug";
  flags[19] = "--help";
  flags[20] = NULL;
  return (flags);
}

char	**flags_options()
{
  char	**flags;

  if ((flags = malloc(sizeof(char **) * 21)) == NULL)
    return (NULL);
  flags[0] = "-l";
  flags[1] = "--level=";
  flags[2] = "-kl";
  flags[3] = "--key-left=";
  flags[4] = "-kr";
  flags[5] = "--key-right=";
  flags[6] = "-kt";
  flags[7] = "--key-turn=";
  flags[8] = "-kd";
  return (flags_options_next(flags));
}

int	option_find(char **av, int it[], s_opt *option)
{
  if (my_strlen(av[it[0]]) <= 3 && my_strstr(av[it[0]], "-w", 2) != 1
      && my_strstr(av[it[0]], "-d", 2) != 1)
    {
      if (av[it[0] + 1] != NULL &&
	  manage_options_next(av[it[0]], av[it[0] + 1], option, it[1]) == -1)
	return (-1);
      it[0]++;
    }
  else
    {
      if (manage_options_next(av[it[0]], NULL, option, it[1]) == -1)
	return (-1);
    }
  return (it[0]);
}

int	manage_options(int ac, char **av, s_opt *option)
{
  int	i;
  char	**flags;
  int	stop;
  int	t;
  int	it[1];

  if ((i = 1) == 1 && (flags = flags_options()) == NULL)
    return (-1);
  while (i < ac && (stop = 0) == 0 && (t = 0) == 0)
    {
      while (stop == 0 && t < 20)
	{
	  if (my_debstrstr(av[i], flags[t], my_strlen(flags[t])) == 1)
	    {
	      if ((it[0] = i) == i && (it[1] = t) == t &&
		  (i = option_find(av, it, option)) == -1)
		return (-1);
	      stop = 1;
	    }
	  t++;
	}
      exit_function(t, av[0]);
      i++;
    }
  return (0);
}
