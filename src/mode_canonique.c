/*
** mode_canonique.c for mode_canonique in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Mar  8 13:09:34 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 18:54:39 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	mode_canonique_spec(int pass)
{
  static struct termios	oldt;
  static struct termios	newt;

  if (pass == 0)
    {
      ioctl(0, TCGETS, &oldt);
      ioctl(0, TCGETS, &newt);
      newt.c_lflag &= ~ECHO;
      newt.c_lflag &= ~ICANON;
      ioctl(0, TCSETS, &newt);
    }
  else if (pass == 1)
    ioctl(0, TCSETS, &oldt);
}

void	mode_canonique(int pass)
{
  static struct termios	oldt;
  static struct termios	newt;

  if (pass == 0)
    {
      ioctl(0, TCGETS, &oldt);
      ioctl(0, TCGETS, &newt);
      newt.c_lflag &= ~ECHO;
      newt.c_lflag &= ~ICANON;
      newt.c_cc[VMIN] = 0;
      newt.c_cc[VTIME] = 7;
      ioctl(0, TCSETS, &newt);
    }
  else if (pass == 1)
    ioctl(0, TCSETS, &oldt);
}

char	*parse_name_next(char *s, int i, int t)
{
  char	*s1;
  char	*s2;

  s1 = NULL;
  s1 = my_dup_strcpy(s1, s);
  while (t < i)
    {
      t++;
      s1++;
    }
  if ((s2 = NULL) == NULL && my_strstr(s1, ".tetrimino", 10) == 1)
    {
      t = 0;
      s2 = malloc(sizeof(char *) * (i + 1));
      while (t < i)
	{
	  s2[t] = s[t];
	  t++;
	}
      s2[t] = '\0';
      return (s2);
    }
  else
    return (parse_name(s, i + 1));
  return (NULL);
}

char	*parse_name(char *s, int i)
{
  int	t;
  int	len;

  t = 0;
  len = 0;
  if (i >= my_strlen(s))
      return (NULL);
  while (s && s[i] != '\0' && s[i] != '.')
    i++;
  t = i;
  while (s && s[t] != '\0' && s[t])
    {
      t++;
      len++;
    }
  if (len == 10)
    return (parse_name_next(s, i, 0));
  else
    return (parse_name(s, i + 1));
  return (s);
}
