/*
** colison.c for colision.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Mar 15 22:31:57 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 02:38:08 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	manage_opt_other(s_opt *option, int pass)
{
  if (pass == 1)
    option->debug_mode = 1;
  else if (pass == 0)
    option->hide_next = 0;
}

void	sigintHandler(int sign)
{
  if (sign == SIGINT)
    return ;
}

int		check_bottom(s_disp *disp, int *quad)
{
  int		i;
  int		t;
  static int	u = 0;

  t = 0;
  i = ((disp->info_map->x_sup - disp->info_map->x_inf + 1) * (disp->y +disp->
      form->haut - disp->info_map->y_sup)) + (disp->x - disp->info_map->x_inf);
  if (u == 1)
    {
      u = 0;
      return (-1);
    }
  while (disp->form->form[disp->form->haut - 1][t] != '\0')
    {
      if (disp->form->form[disp->form->haut - 1][t] == ' ' && quad[i] == 0)
	{
	  if (disp->form->form[disp->form->haut - 2][t] == '*' && quad[i] == 0)
	    u = 1;
	}
      else if (disp->form->form[disp->form->haut - 1][t] == '*' && quad[i] == 0)
	return (-1);
      t++;
      i++;
    }
  return (0);
}

int	exit_function(int i, char *s)
{
  if (i >= 20)
    aff_help(s);
  if (i == -10)
    exit(-1);
  return (0);
}

void	disp_form_debug(char *s)
{
  int	i;
  int	stop;

  i = 0;
  stop = my_strlen(s) - 1;
  while (s && s[stop] != '\0' && stop >= 0 && s[stop] == ' ')
    stop--;
  while (s && i <= stop)
    {
      my_putchar(s[i]);
      i++;
    }
  my_putchar('\n');
}
