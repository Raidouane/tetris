/*
** my_strcpy.c for my_strcpy.c in /home/el-mou_r/rendu/bootstraps/PSU_2015_my_exec
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Jan 17 19:10:22 2016 Raidouane EL MOUKHTARI
** Last update Fri Mar 18 20:22:22 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src && src[i] != '\0')
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = '\0';
  return (dest);
}

char	*my_dup_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  if ((dest = malloc(sizeof(char *) * (my_strlen(src) + 1))) == NULL)
    return (NULL);
  while (src && src[i] != '\0')
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = '\0';
  return (dest);
}
