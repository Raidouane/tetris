/*
** display_next.c for display_next in /home/maurin_b/rendu/PSU_2015_tetris/thomas/test/PSU_2015_tetris/src
** 
** Made by thomas maurin
** Login   <maurin_b@epitech.net>
** 
** Started on  Wed Mar  2 15:44:55 2016 thomas maurin
** Last update Fri Mar 18 15:14:57 2016 thomas maurin
*/

#include "my.h"

void	display_next_col(t_form size, s_imap inf)
{
  int	pos_x;
  int	pos_y;
  int	save_y;
  int	haut_next;

  pos_x = inf.x_sup + 10;
  pos_y = inf.y_sup + 1;
  save_y = pos_y;
  haut_next = (size.haut + 10) + pos_y;
  while (pos_y <= haut_next)
    {
      mvprintw(pos_y, pos_x ,"|");
      pos_y += 1;
    }
  pos_x += size.larg + save_y ;
  while (save_y <= haut_next)
    {
      mvprintw(save_y, pos_x ,"|");
      save_y += 1;
    }
}

void	display_next_up_next(int *i,  int pos_x, int pos_y)
{
  while (*i <= 4)
    {
      mvprintw(pos_y , pos_x + (*i) ,"N");
      (*i)++;
      mvprintw(pos_y , pos_x + (*i) ,"e");
      (*i)++;
      mvprintw(pos_y , pos_x + (*i) ,"x");
      (*i)++;
      mvprintw(pos_y , pos_x + (*i) ,"t");
      (*i)++;
    }
}

void	display_next_up(int *i,  t_form size, s_imap inf)
{
  int	pos_x;
  int	pos_y;
  int	size_next;

  pos_x = inf.x_sup + 10;
  pos_y = inf.y_sup;
  size_next = (size.larg + 10) + 3;
  while (*i <= size_next)
    {
      if (*i == 0)
	{
	  mvprintw(pos_y, pos_x ,"/");
	  (*i)++;
	}
      display_next_up_next(i, pos_x, pos_y);
      while (*i <= size_next)
	{
	  mvprintw(pos_y , pos_x + (*i) ,"-");
	  (*i)++;
	}
      mvprintw(pos_y , pos_x + (*i) ,"\\");
    }
}

void	display_next(t_form size, s_imap inf)
{
  int	i;
  int	pos_x;
  int	pos_y;

  i = 0;
  pos_x = inf.x_sup + 2;
  pos_y = inf.y_sup + 2;
  display_next_up(&i, size, inf);
  display_next_col(size, inf);
  mvprintw(pos_y ,pos_x + i , "\n");
  i++;
  mvprintw(pos_y ,pos_x + i , "\n");
  i++;
  mvprintw(pos_y , pos_x + i ,"\n");
}
