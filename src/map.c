/*
** map.c for map.c in /home/maurin_b/rendu/PSU_2015_tetris
** 
** Made by thomas maurin
** Login   <maurin_b@epitech.net>
** 
** Started on  Mon Feb 22 14:53:02 2016 thomas maurin
** Last update Sun Mar 20 17:11:10 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

void	manage_colision(s_disp *disp, t_aff *tmp, int *quad)
{
  int	xy[2];

  disp->aff = tmp;
  disp->aff = list_aff(disp->aff, disp->form, disp->x, disp->y);
  xy[0] = disp->x;
  xy[1] = disp->y;
  maj_quad(disp->form, xy, disp->info_map, quad);
  disp->form = disp->form->next;
  disp->y = disp->info_map->y_sup + 1;
  disp->x = disp->info_map->x_sup - (disp->opt.map_col / 2);
}

int		display_map(s_disp *disp, int *quad, t_aff *aff, t_aff *tmp)
{
  char		touch[256];

  if ((tmp = NULL) == NULL && (aff = NULL) ==
      NULL && disp_map(disp->opt, disp->info_map, disp) == -1)
    return (-1);
  disp->y = disp->info_map->y_sup + 1;
  disp->x = disp->info_map->x_sup - (disp->opt.map_col / 2);
  while (42 && (touch[0] = 0) == 0)
    {
      disp->aff = aff;
      if (display(disp, tmp, quad) == -1)
	return (0);
      touch[read(0, touch, 255)] = '\0';
      if ((disp->quad = quad) != quad || touch[0] == disp->opt.keyquit[0])
	return (0);
      if (check_bottom(disp, quad) == 0)
	manage_keys(touch, disp, disp->form, disp->info_map->x_sup);
      else
	{
	  manage_colision(disp, tmp, quad);
	  tmp = disp->aff;
	}
      disp_map_2(disp);
    }
  return (0);
}

void		aff_debugg(t_form *tetrim)
{
  t_form	*tmp;
  int		i;

  tmp = tetrim->prev;
  while (tetrim != tmp)
    {
      i = 0;
      my_printf("Tetriminos : Name %s : Size %d*%d : Color %d :\n"
		, tetrim->name, tetrim->larg, tetrim->haut, tetrim->color);
      while (tetrim->form[i] != NULL)
  	{
	  disp_form_debug(tetrim->form[i]);
  	  i++;
  	}
      tetrim = tetrim->next;
    }
  i = 0;
  my_printf("Tetriminos : Name %s : Size %d*%d : Color %d :\n", tetrim->name,
	    tetrim->larg, tetrim->haut, tetrim->color);
  while (tetrim->form[i] != NULL)
    {
      disp_form_debug(tetrim->form[i]);
      i++;
    }
  my_printf("Press a key to start Tetris \n");
}

t_form		*init_game(s_opt *option)
{
  t_form	*tetrim;
  char		c;

  if (option->map_col == 0)
    option->map_col = 10;
  if (option->map_row == 0)
    option->map_row = 20;
  if ((tetrim = recup_tetriminos(option->debug_mode)) == NULL)
    return (NULL);
  if (option->debug_mode == 1)
    {
      aff_debugg(tetrim);
      mode_canonique_spec(0);
      read(0, &c, 1);
      mode_canonique_spec(1);
    }
  return (tetrim);
}

void	window(s_opt option)
{
  SCREEN	*win;
  t_form	*tetrim;
  s_imap	info_map;
  s_disp	disp;
  t_aff		*aff;

  if ((tetrim = init_game(&option)) == NULL)
    return ;
  my_putstr(tigetstr("smkx"));
  win = newterm(NULL, stderr, stdin);
  noecho();
  start_color();
  cbreak();
  curs_set(0);
  signal(SIGINT, sigintHandler);
  maj_struct_disp(&disp, option,  int_map(option), tetrim);
  disp.info_map = &info_map;
  (void)aff;
  display_map(&disp, int_map(option), NULL, NULL);
  mode_canonique(1);
  clear();
  endwin();
  delscreen(win);
  return ;
}
