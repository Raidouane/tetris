/*
** option_next2.c for option_next2.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Mar 18 00:57:45 2016 Raidouane EL MOUKHTARI
** Last update Fri Mar 18 21:06:52 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	racc_opts(char *s, char *s1, s_opt *option, int t)
{
  int	var;

  var = 0;
  if (my_strstr(s, "-l", 2) == 1)
    var = manage_opt_level(s, s1, 2, option);
  else
    var = manage_key(s, s1, option, t);
  if (var == -1)
    return (-1);
  return (0);
}

int	aff_help(char *s)
{
  my_printf("Usage: %s [options]\n", s);
  my_putstr("Options:\n");
  my_putstr("--help\t\t\tDisplay this help\n");
  my_putstr("-l --level={num}\tStart Tetris at level num\n");
  my_putstr("-kl --key-left={K}\tMove tetrimino on LEFT with key K\n");
  my_putstr("-kr --key-right={K}\tMove tetrimino on RIGHT with key K\n");
  my_putstr("-kt --key-turn={K}\tTurn tetrimino with key K\n");
  my_putstr("-kd --key-drop={K}\tSet default DROP on key K\n");
  my_putstr("-kq --key-quit={K}\tQuit program when press key K\n");
  my_putstr("-kp --key-pause={K}\tPause and restart game when press key K\n");
  my_putstr("--map-size={row,col}\tSet game size at row, col\n");
  my_putstr("-w --without-next\tHide next tetrimino\n");
  my_putstr("-d --debug\t\tDebug mode\n");
  exit(1);
}
