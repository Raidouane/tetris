/*
** tools_2.c for tools_2.c in /home/el-mou_r/rendu/PSU/tetris/PSU_2015_tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Feb 25 01:51:20 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 17:32:29 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	check_line(char *s)
{
  int	i;

  i = 0;
  while (s && s[i] != '\0')
    {
      if (s[i] != ' ' && s[i] != '*')
	return (-1);
      i++;
    }
  return (0);
}

int	my_strlen_spaces(char *s, int larg)
{
  int	i;
  int	len;

  len = 0;
  i = 0;
  while (s && s[i] != '\0')
    {
      if (i < larg)
	len++;
      else if (i >= larg && s[i] != ' ')
	len++;
      i++;
    }
  if (len != larg)
    return (-1);
  return (1);
}

int	loose(int *quad, int larg)
{
  int	i;

  i = larg + 1;
  while (i < (larg * 2) - 1)
    {
      if (quad[i] == 0)
	return (-1);
      i++;
    }
  return (0);
}

void	maj_struct_disp(s_disp *disp, s_opt option, int *quad, t_form *form)
{
  disp->opt = option;
  disp->quad = quad;
  disp->form = form;
}

void	disp_map_2(s_disp *disp)
{
  clear();
  tet_aff();
  display_col(disp->opt.map_col, disp->l - 1, disp->c, disp->info_map);
  display_line(disp->l, disp->opt, disp->c, disp->info_map);
}
