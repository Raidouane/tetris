##
## Makefile for Makefile in /home/el-mou_r/rendu/PSU/tetris
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Mon Feb 22 14:21:19 2016 Raidouane EL MOUKHTARI
## Last update Sun Mar 20 22:51:36 2016 Raidouane EL MOUKHTARI

CC	= gcc

RM	= rm

NAME	= tetris

CFLAGS  = -I./include -W -Wall -Werror -pedantic -D _BSD_SOURCE

SRC	=src/tetris.c			\
	src/map.c			\
	src/map2.c			\
	src/aff.c			\
	src/check_infos.c		\
	src/check_tetriminos.c		\
	src/check_error.c		\
	src/manage_option_level.c	\
	src/option_next.c		\
	src/option_next2.c		\
	src/register_tetrimino.c	\
	src/display_next.c		\
	src/get_next_line.c		\
	src/manage_option_key.c		\
	src/init_my_struct_options.c	\
	src/option.c			\
	src/tools.c			\
	src/tools_2.c			\
	src/my_strcat.c			\
	src/my_strcpy.c			\
	src/my_strlen.c			\
	src/my_putchar.c		\
	src/my_putstr.c			\
	src/my_getnbr.c			\
	src/int_map.c			\
	src/maj_quad.c			\
	src/my_strstr.c			\
	src/colison.c			\
	src/my_doublestr_cpy.c		\
	src/mode_canonique.c

SRCMYP  =my_printf/src/my_putchar.c		\
        my_printf/src/my_printf.c		\
        my_printf/src/my_putstr.c		\
        my_printf/src/my_put_nbr.c		\
        my_printf/src/my_putbase.c		\
        my_printf/src/my_putbase_s.c		\
        my_printf/src/my_putbase_pointer.c	\
        my_printf/src/flago.c			\
        my_printf/src/flagsbpxx.c		\
        my_printf/src/my_getnbr.c		\
        my_printf/src/my_put_nbr_u.c		\
        my_printf/src/my_putstr_s.c		\
        my_printf/src/my_revstr.c		\
        my_printf/src/flag_iudcs.c		\
        my_printf/src/my_strlen.c		\
        my_printf/src/my_check_of_printf.c

OBJ	= $(SRC:.c=.o)

OBJMYP  = $(SRCMYP:.c=.o)

$(NAME): 	$(OBJ) $(OBJMYP)
	ar rc my_printf.a $(OBJMYP)
	ranlib my_printf.a
	$(CC) $(OBJ) my_printf.a -lncurses -o $(NAME)

all:	$(NAME)

clean:
	$(RM) -f $(OBJ)

fclean:	clean
	$(RM) -f $(NAME)

re:	fclean all
