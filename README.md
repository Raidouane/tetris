# Tetris

Made in 2015.

The goal of this project is to recreate the Tetris game in a UNIX terminal, with the Gameboy version rules, using [ncurses](https://linux.die.net/man/3/ncurses).

The folder `tetriminos` in the root directory contains files that describe the game pieces.

These files are composed in the following way:
 - on the first line, the size and color of the piece in this format: width height color_code\n (the number
of the color corresponds to the ncurses capacity’s color numbers),
 - on the h following lines (where h is the height of the tetrimino), the piece’s shape composed with
asterisks (*) and spaces (‘ ’).

## Requirements

 - [A Unix distribution](https://www.lifewire.com/unix-flavors-list-4094248)
 - [Make](https://www.gnu.org/software/make/)
 - [Ncurses lib](https://linux.die.net/man/3/ncurses).
 - [GCC](https://gcc.gnu.org/)

## Compilation

In the the root of the repository, run `$> make` to build the `./tetris` executable file.

## How to use Tetris?

 - Compile the executable file (see **Compilation** above).
 ```
 $> ./tetris --help
 Usage: ./tetris [options]
 Options:
 --help Display this help
 -L --level={num} Start Tetris at level num (def: 1)
 -l --key-left={K} Move the tetrimino LEFT using the K key (def: left arrow)
 -r --key-right={K} Move the tetrimino RIGHT using the K key (def: right arrow)
 -t --key-turn={K} TURN the tetrimino clockwise 90d using the K key (def: top
 arrow)
 -d --key-drop={K} DROP the tetrimino using the K key (def: down arrow)
 -q --key-quit={K} QUIT the game using the K key (def: ‘q’ key)
 -p --key-pause={K} PAUSE/RESTART the game using the K key (def: space bar)
 --map-size={row,col} Set the numbers of rows and columns of the map (def: 20,10)
 -w --without-next Hide next tetrimino (def: false)
 -D --debug Debug mode (def: false)
 ```


## Author

* **Raidouane EL MOUKHTARI** ([LinkedIn](https://www.linkedin.com/in/raidouane-el-moukhtari/) / [Gitlab](https://gitlab.com/Raidouane))
