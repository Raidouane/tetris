/*
** my_putbase_pointer.c for my_putbase_pointer.c in /home/el-mou_r/rendu/PSU_2015_my_printf
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Nov  8 20:46:24 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 15 01:23:55 2015 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include "../include/my.h"

char	*my_getbase_pointer(long int nb, char *base_str, t_struct *st)
{
  char	*final;
  int	resultat;
  int	i;
  int	j;
  int	base;

  i = 0;
  base = my_strlen(base_str);
  final = malloc(sizeof(char)*(base + 1));
  if (final == NULL)
    return (NULL);
  while (nb != 0)
    {
      final[i] = base_str[nb % base];
      nb = nb / base;
      i++;
    }
  final[i] = '\0';
  my_revstr(final);
  my_putstr(final, st);
  free(final);
}
