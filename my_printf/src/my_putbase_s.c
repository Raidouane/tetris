/*
** my_putbase_s.c for my_putbase_S.c in /home/el-mou_r/rendu/PSU_2015_my_printf
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Nov  8 20:46:24 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 15 01:24:27 2015 Raidouane EL MOUKHTARI
*/


#include <stdlib.h>
#include "../include/my.h"

char	*my_getbase_S(unsigned int nb, char *base_str)
{
  char	*final;
  int	resultat;
  int	i;
  int	j;
  int	base;

  i = 0;
  base = my_strlen(base_str);
  final = malloc(sizeof(char)*(base + 1));
  if (final == NULL)
    return (NULL);
  while (nb != 0)
    {
      final[i] = base_str[nb % base];
      nb = nb / base;
      i++;
    }
  final[i] = '\0';
  final = my_revstr(final);
  return (final);
}
