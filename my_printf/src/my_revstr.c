/*
** my_revstr.c for my_revstr.c in /home/el-mou_r/rendu/PSU_2015_my_printf/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Nov 12 20:55:53 2015 Raidouane EL MOUKHTARI
** Last update Thu Nov 12 20:58:10 2015 Raidouane EL MOUKHTARI
*/

#include "../include/my.h"

char	*my_revstr(char *str)
{
  int	e;
  int	c;
  char	t;
  char	a;

  t = 0;
  c = my_strlen(str);
  e = c - 1;
  while (t != (c / 2))
    {
      a = str[t];
      str[t] = str[e];
      str[e] = a;
      t = t + 1;
      e = e - 1;
    }
  return (str);
}
