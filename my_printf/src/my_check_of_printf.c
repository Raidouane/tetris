/*
** my_check_of_printf.c for my_check_of_printf.c in /home/el-mou_r/rendu/pr/last/PSU_2015_my_printf/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Nov 12 01:50:21 2015 Raidouane EL MOUKHTARI
** Last update Fri Nov 13 21:22:50 2015 Raidouane EL MOUKHTARI
*/

#include "../include/my.h"
#include <stdarg.h>

void	next_manage_$(t_struct *st, char *s)
{
  int	nb;

  nb = 1;
  if (s[st->i] >= '0' && s[st->i] <= '9')
    nb = my_getnbr(&s[st->i]);
  while (nb != 1)
    {
      va_arg(st->va, int);
      nb--;
    }
  st->i = st->i + 2;
}

void	check_$(t_struct *st, char *s)
{
  int	i;
  int	inc;

  inc = 0;
  i = st->i;
  while (s[i] >= '0' && s[i] <= '9')
    i++;
  if (s[i] == '$')
    {
      if (s[st->i -1] != '$')
        next_manage_$(st, s);
      st->restart = 1;
      va_end(st->va);
      st->pass = 1;
    }
  else if (s[i] != '$')
    {
      while (st->flag_model[inc] != '\0' && st->flag_model[inc] != s[i])
        inc++;
      if (inc > 11)
        my_putchar('%', st);
    }
}

void	check_tour(t_struct *st, char *s)
{
  int	i;

  i = 0;
  while (i != st->nb_tour && st->pass != 1)
    {
      va_arg(st->va, int);
      i++;
    }
}
