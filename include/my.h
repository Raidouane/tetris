/*
** my.h for my.h in /home/el-mou_r/rendu/PSU/tetris
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Feb 22 14:25:50 2016 Raidouane EL MOUKHTARI
** Last update Sun Mar 20 22:02:42 2016 Raidouane EL MOUKHTARI
*/

#ifndef MY_H_
# define MY_H_

# include <ncurses.h>
# include <ncurses/curses.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/types.h>
# include <dirent.h>
# include <signal.h>
# include <sys/stat.h>
# include <term.h>
# include <fcntl.h>
# include <termios.h>
# include <sys/ioctl.h>
# include "get_next_line.h"
# include "my_printf.h"

typedef struct	s_option
{
  int		level;
  char		*keyright;
  char		*keyleft;
  char		*keyturn;
  char		*keyquit;
  char		*keydrop;
  char		*keypause;
  int		map_row;
  int		map_col;
  int		hide_next;
  int		debug_mode;
  int		kr;
  int		kl;
  int		kt;
  int		kd;
  char		*bin;
}		s_opt;

typedef struct	s_info
{
  char		*name;
  int		haut;
  int		debug;
  int		larg;
  int		color;
  int		val;
}		s_infos;

typedef	struct		s_map
{
  int			x_inf;
  int			x_sup;
  int			y_inf;
  int			y_sup;
}			s_imap;

typedef struct		s_tetri
{
  char			*name;
  int			id;
  int			turned;
  int			haut;
  int			larg;
  int			color;
  char			**form;
  struct s_tetri	*prev;
  struct s_tetri	*next;
}			t_form;

typedef struct		s_display_tet
{
  int			color;
  char			**form;
  int			x;
  int			y;
  struct s_display_tet	*next;
  struct s_display_tet	*prev;
}			t_aff;

typedef	struct		s_display
{
  int			x;
  int			y;
  s_opt			opt;
  s_imap		*info_map;
  int			*quad;
  t_form		*form;
  t_aff			*aff;
  int			l;
  int			c;
  int			pause;
}			s_disp;

int	check_error_form_third(int *k, int haut, int *h, int *pass);
int	check_error_form_second(char *s, int lh[], int *k, char **form);
int	check_error_form(s_infos *infos, char *s, char **form);
int	do_while_open_tet(int fd, t_form **tetrim, char **form, s_infos *infos);
t_form	*fill_list_form(t_form **la, s_infos infos, char **form, int pass);
int     size_doublestring(char **form);
t_form	*fill_list_last_step(t_form *tmp, t_form *la);
void	fill_level_option(char *s, int t, char *nb, s_opt *option);
int	manage_opt_next_level(char *s, s_opt *option);
int	manage_opt_level(char *s, char *s1, int ac, s_opt *option);
void	my_putchar(char );
int	my_strlen(char *s);
void	my_putstr(char *s);
int	my_debstrstr(char *s1, char *s2, int n);
int	my_strstr(char *s1, char *s2, int n);
int	manage_key(char *s, char *s1, s_opt *option, int key);
int	manage_opt_next_key(char *s, s_opt *option, int key);
void	fill_keyoption(char s[], s_opt *option, int key);
int	check_num(char *s);
int	my_getnbr(char *s);
int	init_options(s_opt *opt);
void	aff_debug(s_opt *option);
int	manage_options(int ac, char **av, s_opt *option);
int	check_num(char *);
int	center_line(int line);
int	center_col(int col);
void	display_col(int col, int fin, int depart, s_imap *);
void	display_line(int l, s_opt option, int c, s_imap *);
void	window(s_opt option);
int	display_map(s_disp *, int *, t_aff *, t_aff *);
t_form	*recup_tetriminos(int);
int	open_tetriminos(char *s, t_form **tetrim, int debug);
int	check_error_tetriminos(char *s, int k, int i, t_form **tetrim);
int	check_info_tetrimino(char *s, s_infos *infos);
int	parse_infos(char *s, int *larg, int *haut, int *color);
void	attribute_infos(char *nb, int *larg, int *haut, int *color);
int	count_spaces(char *s);
int	len_malloc(char*s, int i);
int	all_num_spaces(char *s);
char	*gnl_else(int i, char *s, char **save, int t);
char	*gnl(char **save);
char	*check_if_all_is_correct(char *s, char *save, const int fd);
char	*get_next_line(const int fd);
char	*check_fd_changed(char *save, int fd);
char	*my_strcat(char *dest, char *src);
char	*my_strcpy(char *dest, char *src);
char	*my_dup_strcpy(char *dest, char *src);
char	*concatain_open(char *s);
int	check_line(char*s);
char	**my_double_str_cpy(char **dest, char **src);
void	mode_canonique(int pass);
void	mode_canonique_spec(int pass);
int	size_doublestring(char **form);
int	*int_map(s_opt option);
int	maj_quad(t_form *form, int xy[], s_imap *info_map, int *quad);
int	check_bottom(s_disp *disp, int *quad);
int	my_strlen_spaces(char *s, int larg);
int	loose(int *quad, int larg);
void	loose_aff();
t_aff	*aff_tet(t_aff *, int, int, int);
void	aff_debug_next(s_opt *option);
void	aff_debug(s_opt *option);
int	fill_resize_option(char *s, int i, s_opt *option, int size);
int	manage_resize(char *s, s_opt *option);
void	manage_opt_other(s_opt *option, int pass);
int	racc_opts(char *s, char *s1, s_opt *option, int t);
int	aff_help(char *s);
int	center_line(int line);
int	center_col(int col);
void	display_col(int col, int fin, int depart, s_imap *info_map);
void	display_line(int l, s_opt option, int c, s_imap *info_map);
int	disp_map(s_opt option, s_imap *info_map, s_disp *disp);
t_aff	*list_aff(t_aff *la, t_form *form, int x, int y);
void	aff_first_tet(t_aff *aff, int a, int u, int d);
t_aff	*aff_tet(t_aff *aff, int a, int u, int d);
int	manage_keys(char touch[], s_disp *disp, t_form *form, int x_sup);
int	display(s_disp *disp, t_aff *tmp, int *quad);
void	maj_struct_disp(s_disp *disp, s_opt option, int *quad, t_form *form);
void	disp_map_2(s_disp *disp);
void	sigintHandler(int sign);
void	my_putstr_spec(char *str);
int	exit_function(int i, char *);
void	disp_form_debug(char *s);
int	my_strlen_tet(char *str);
char	*parse_name(char *s, int i);
int	parse_name_nb(char *s, int i);
void	pause_tet();
void	tet_aff();

#endif /* !MY_H_ */
